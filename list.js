import { getLocalStorage, setNewLocalStorage } from "./localStorage.js";

window.addEventListener("load", () => {
  let localStorageData;
  const listItemsContainer = document.getElementById("list-items-container");
  const inputDelete = document.getElementById("input-delete");
  const btnDelete = document.getElementById("btn-delete");
  const listItem = document.createElement("tr");

  const deleteUser = () => {
    const id = inputDelete.value;

    if (id === "") {
      alert("Campo CPF não pode ser vazio.");
      return;
    }

    const localData = getLocalStorage("users", true);
    const newItems = localData.filter((item) => item.cpf !== id);

    if(newItems.length === localData.length) {
      alert("Não há itens para excluir.")
    }

    setNewLocalStorage("users", newItems);
    updateHTML(newItems);
  };

  const createHTML = () => {
    listItemsContainer.innerHTML = "";

    localStorageData.forEach((user) => {
      listItemsContainer.innerHTML += `<tr><td>${user.name}</td>
      <td>${user.cpf}</td>
      <td>${user.phone}</td>
      <td>${user.email}</td></tr>`;
      listItemsContainer.appendChild(listItem);
    });

    inputDelete.focus();
  };

  const updateHTML = (newHTML) => {
    listItemsContainer.innerHTML = "";

    newHTML.forEach((user) => {
      listItemsContainer.innerHTML += `<tr><td>${user.name}</td>
      <td>${user.cpf}</td>
      <td>${user.phone}</td>
      <td>${user.email}</td></tr>`;
      listItemsContainer.appendChild(listItem);
    });

    inputDelete.value = "";
    inputDelete.focus();
  };

  localStorageData = getLocalStorage("users", true);
  btnDelete.addEventListener("click", deleteUser);

  createHTML();
});
