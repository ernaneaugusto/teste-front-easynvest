const getLocalStorage = (key, isParse = false) => {
  const items = localStorage.getItem(key);
  if (!isParse) {
    return items;
  }
  return JSON.parse(items);
};

const setNewLocalStorage = (key, newStorageParse) => {
  localStorage.setItem(key, JSON.stringify(newStorageParse));
};

export { getLocalStorage, setNewLocalStorage };
