import { getLocalStorage } from "./localStorage.js";

window.addEventListener("load", () => {
  const inputName = document.getElementById("name");
  const inputCpf = document.getElementById("cpf");
  const inputPhone = document.getElementById("phone");
  const inputEmail = document.getElementById("email");
  const btnSubmit = document.getElementById("button");
  const loader = document.getElementById("loader");
  btnSubmit.disabled = true;

  setTimeout(() => {
    validBtnSubmit();
  }, 1000);

  const setLocalStorage = (newUser) => {
    const currentUsers = getLocalStorage("users");
    const currentUsersParse = JSON.parse(currentUsers);

    if (!currentUsers) {
      localStorage.setItem("users", JSON.stringify([newUser]));
      return;
    }

    currentUsersParse.push(newUser);
    localStorage.setItem("users", JSON.stringify(currentUsersParse));
    loader.classList.add("active");

    setTimeout(() => {
      window.location.href = "list-users.html";
    }, 1000);
  };

  inputName.addEventListener("focusout", () => {
    if (inputName.value.length === 0) {
      inputName.classList.add("invalid");
      validBtnSubmit();
      return;
    }
    inputName.classList.remove("invalid");
    validBtnSubmit();
  });

  inputCpf.addEventListener("focusout", () => {
    if (inputCpf.value.length === 0) {
      inputCpf.classList.add("invalid");
      validBtnSubmit();
      return;
    }
    validBtnSubmit();
    inputCpf.classList.remove("invalid");
  });

  inputPhone.addEventListener("focusout", () => {
    if (inputPhone.value.length === 0) {
      inputPhone.classList.add("invalid");
      validBtnSubmit();
      return;
    }
    inputPhone.classList.remove("invalid");
    validBtnSubmit();
  });

  inputEmail.addEventListener("focusout", () => {
    if (inputEmail.value.length === 0) {
      inputEmail.classList.add("invalid");
      validBtnSubmit();
      return;
    }
    inputEmail.classList.remove("invalid");
    validBtnSubmit();
  });

  btnSubmit.addEventListener("click", (event) => {
    event.preventDefault();

    setLocalStorage({
      name: inputName.value,
      cpf: inputCpf.value,
      phone: inputPhone.value,
      email: inputEmail.value,
    });
  });

  const validBtnSubmit = () => {
    if (
      inputName.value.length === 0 ||
      inputCpf.value.length === 0 ||
      inputEmail.value.length === 0 ||
      inputPhone.value.length === 0
    ) {
      btnSubmit.disabled = true;
      return;
    }
    btnSubmit.disabled = false;
  };
});

export { getLocalStorage };
