# Teste Front-end Easynvest

Olá, me chamo [Ernane Toledo](https://www.linkedin.com/in/ernanetoledo/) e é um prazer participar desse processo seletivo.

Para rodar o projeto peço que instale a extensão do Live Server no VSCode, basta procurar por "Live Server" na guia de extensões. Após instalado clique com o botão direito do mouse sobre o arquivo index.html e selecione a opção "Open with Live Server", ou clique no botão "Go Live" que aparecerá no canto direito inferior do VSCode. Qualquer dúvida tem mais informações [nesse link](https://github.com/ritwickdey/vscode-live-server) :)

Eu utilizei o Sass para trablhar com os estilos no projeto. O arquivo de estilo principal `assets/style.css` já está todo pré-processado, mas caso queira executar o Sass basta executar o comando `npm i -g sass` para instalar o Sass. Com o pasta do projeto aberta, execute o comando `sass --watch asets:assets` para ficar "escutando" as alterações dos arquivos .scss dentro dessa pasta.

Por hora é isso, qualquer coisa estou à disposição :)
#Vlw
